package com.ebank.dao;

import java.util.List;

import com.ebank.beans.ComplaintsBean;

public interface ComplaintsDao {


List<ComplaintsBean> fetchTransactions(String complaint, int accNum);



boolean writeComplaintIntoTable(ComplaintsBean complaint);



List<ComplaintsBean> getAllUserComplaints();
}
