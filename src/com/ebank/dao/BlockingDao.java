package com.ebank.dao;

public interface BlockingDao {
public boolean readUserStatus(int AccountNumber);
public boolean updateUserStatus(int AccountNumber,boolean flag);
public boolean isUserBlocked(int accountNumber);
}
