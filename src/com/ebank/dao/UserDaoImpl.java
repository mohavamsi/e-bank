package com.ebank.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ebank.beans.AccountBean;
import com.ebank.beans.UserBean;
import com.ebank.utility.DButil;

/**
 * This class contains user functionalities which deals with databases.
 * 
 * @author BATCH-E
 *
 */
public class UserDaoImpl implements UserDAO {
	/**
	 * This method is used to verify whether a user is present or not in user table.
	 */
	public boolean isRecordExists(int userId, String userPassword) {
		boolean flag = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		UserBean user = new UserBean();
		// this query dislays userid and passsword for specified user.
		String sql = ("select userId,password from user where userId=?");
		try {
			// connection is intiliazed from getcon() in dbutil class.
			con = DButil.getcon();
			// performing db operations.
			ps = con.prepareStatement(sql);
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			// checking whether the given values are present in table or not.
			while (rs.next()) {
				int userID = rs.getInt(1);
				String password1 = rs.getString(2);
				if (userId == (userID) && userPassword.equals(password1)) {
					flag = true;
				} else
					flag = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;
	}

	/**
	 * This method returns true if userId is present in the database
	 */
	public boolean doLogin(int userId) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// this query is used to get userid from user table
		String sql = "select userId from user where userId = ?";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * this method is used to insert user data into the table.
	 */
	public boolean insertUserRecord(UserBean user) {

		Connection con = null;
		PreparedStatement ps = null;

		boolean flag = false;
		// this query is used to insert the data into the table.
		String sql = ("insert into user values (?,?,?,?,?,?,?,?)");
		try {
			// connection is intiliazed from getcon() in dbutil class.
			con = DButil.getcon();
			// performing db operations.
			ps = con.prepareStatement(sql);
			ps.setInt(1, user.getUserId());
			ps.setString(2, user.getFirstName());
			ps.setString(3, user.getLastName());
			ps.setString(4, user.getPassword());
			ps.setString(5, user.getPhoneNum());
			ps.setString(6, user.getMailId());
			ps.setDate(7, new Date(user.getDateOfBirth().getTime()));
			ps.setString(8, user.getGender());
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				flag = true;
			} else {
				System.out.println("Record is not inserted");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;
	}

	/**
	 * This method is used to display all the users present in the given table
	 */
	public List<UserBean> getAllUsers() {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		UserBean user = null;
		List<UserBean> userlist = new ArrayList<UserBean>();
		// this query is used to display all the users present in the user table
		String sql = "select * from user ";

		try {
			con = DButil.getcon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				user = new UserBean();
				user.setUserId(rs.getInt(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setPassword(rs.getString(4));
				user.setPhoneNum(rs.getString(5));
				user.setMailId(rs.getString(6));
				user.setDateOfBirth(rs.getDate(7));
				user.setGender(rs.getString(8));
				userlist.add(user);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return userlist;
	}

	/**
	 * this method is used to display information of user based on userid
	 */
	public UserBean fetchRecordById(int userId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		UserBean user = new UserBean();
		// preparing sql query for updating records
		String sql = "select *   from user where userId = ?";
		try {
			// getCon is a static method which returns Connection Object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			// setting values into the place holders
			ps.setInt(1, userId);
			// executing sql query
			rs = ps.executeQuery();
			// preparing AdminBean object
			if (rs.next()) {
				user.setUserId(rs.getInt(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setPassword(rs.getString(4));
				user.setPhoneNum(rs.getString(5));
				user.setMailId(rs.getString(6));
				user.setDateOfBirth(rs.getDate(7));
				user.setGender(rs.getString(8));

			}
		} catch (Exception e) {
			System.out.println("Exception occured");
		}

		return user;
	} // End of getRecordById

	/**
	 * This method is used to update user password in the table
	 */
	public boolean updatePassword(int userId, String password) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// this query is used to update password in user table for a particular user.
		String sql = "update user set password = ? where userId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, password);
			ps.setInt(2, userId);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * This method is used to update user firstname in the table
	 */

	public boolean updateFirstName(int userId, String firstName) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// this query is used to update firstname in user table for a particular user.
		String sql = "update user set firstName = ? where userId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, firstName);
			ps.setInt(2, userId);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * This method is used to update user lastname in the table
	 */
	public boolean updateLastName(int userId, String lastName) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// this query is used to update lastname in user table for a particular user.
		String sql = "update user set lastName = ? where userId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, lastName);
			ps.setInt(2, userId);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * This method is used to update user phoneNum in the table
	 */
	public boolean updatePhoneNumber(int userId, String phoneNum) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// this query is used to update phoneNumber in user table for a particular user.
		String sql = "update user set phoneNumber = ? where userId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, phoneNum);
			ps.setInt(2, userId);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * This method is used to update user mailId in the table
	 */
	public boolean updateMailId(int userId, String mailId) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// this query is used to update mailId in user table for a particular user.
		String sql = "update user set mailId = ? where userId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, mailId);
			ps.setInt(2, userId);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * This method is used to update user DateOfBirth in the table
	 */
	public boolean updateDateOfBirth(int userId, java.util.Date dateOfBirth) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		UserBean user = new UserBean();
		// this query is used to update dateOfBirth in user table for a particular user.
		String sql = "update user set dateOfBirth = ? where userId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setDate(1, (Date) user.getDateOfBirth());
			ps.setInt(2, userId);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				// closing the connection.
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * This method is used to update user gender in the table
	 */
	public boolean updateGender(int userId, String gender) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		// this query is used to update gender in user table for a particular user.
		String sql = "update user set gender = ? where userId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, gender);
			ps.setInt(2, userId);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				// closing the connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	@Override
	public boolean insertAccountRecord(AccountBean account) {
		Connection con = null;
		PreparedStatement ps = null;

		boolean flag = false;
		// this query is used to insert the data into the table.
		String sql = ("insert into accountdetails values (?,?,?,?,?,?,?,?)");
		try {
			// connection is intiliazed from getcon() in dbutil class.
			con = DButil.getcon();
			// performing db operations.
			ps = con.prepareStatement(sql);
			ps.setInt(1, account.getAccountNumber());
			ps.setString(2, account.getAccountHolderName());
			ps.setString(3, account.getNomineeName());
			ps.setDouble(4, account.getBalance());
			ps.setDate(5, new Date(account.getOpeningDate().getTime()));
			ps.setBoolean(6, account.isStatus());
			ps.setString(7, account.getTypeOfAccount());
			ps.setInt(8, account.getUserId());

			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				flag = true;
			} else {
				System.out.println("Record is not inserted");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;
	}

	/**
	 * This method fetches Account record from database table based on userId
	 */
	@Override
	public AccountBean fetchAccountByUserId(int userId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccountBean account = new AccountBean();
		// preparing sql query for updating records
		String sql = "select accountNum,accountname,nomineename,balance,DateOfopening,status,typeofAccount,userId from accountdetails where userId = ?";
		try {
			// getCon is a static method which returns Connection Object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			// setting values into the place holders
			ps.setInt(1, userId);
			// executing sql query
			rs = ps.executeQuery();
			// preparing AdminBean object
			if (rs.next()) {
				account.setAccountNumber(rs.getInt(1));
				account.setAccountHolderName(rs.getString(2));
				account.setNomineeName(rs.getString(3));
				account.setBalance(rs.getDouble(4));
				account.setOpeningDate(rs.getDate(5));
				account.setStatus(rs.getBoolean(6));
				account.setTypeOfAccount(rs.getString(7));
				account.setUserId(rs.getInt(8));

			}
		} catch (Exception e) {
			System.out.println("Exception occured");
		}
		return account;
	} // End of fetchAccountByUserId

}
