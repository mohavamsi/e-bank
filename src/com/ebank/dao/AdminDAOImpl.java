package com.ebank.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ebank.beans.AdminBean;
import com.ebank.utility.DButil;

/**
 * This class provides implementation for AdminDAO interface
 * 
 * @author Team-E
 *
 */
public class AdminDAOImpl implements AdminDAO {

	/**
	 * This method is implementation of AdminDAO isRecordExist(int amdinId) method.
	 * This method Checks whether record exist with specific adminId or not
	 */
	@Override
	public boolean isRecordExist(int adminId) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean flag = false;
		// preparing sql query for
		String sql = "select * from AdminDetails where adminId = ?";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, adminId);
			// executing sql query
			rs = ps.executeQuery();
			flag = rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	} // End of isRecordExist


	/**
	 * This method is used to verify whether a user is present or not in user table.
	 */
	public boolean isRecordExist(int adminId, String password) {
		boolean flag = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		// this query dislays adminid and passsword for specified user.
		String sql = ("select adminId,password from admindetails where adminId=?");
		try {
			// connection is intiliazed from getcon() in dbutil class.
			con = DButil.getcon();
			// performing db operations.
			ps = con.prepareStatement(sql);
			ps.setInt(1, adminId);
			rs = ps.executeQuery();
			// checking whether the given values are present in table or not.
			while (rs.next()) {
				int adminID = rs.getInt(1);
				String password1 = rs.getString(2);
				if (adminId == (adminID) && password.equals(password1)) {
					flag = true;
				} else
					flag = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;
	}
	
	public boolean doLogin(int adminId) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "select adminId from admindetails where adminId = ?";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, adminId);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				result = true;
			}
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	

	/**
	 * This is the implementation of AdminDAO insertAdminRecord(AdminBean admin)
	 * method. This method inserts a new Admin record into the table.
	 */
	@Override
	public boolean insertAdminRecord(AdminBean admin) {
		Connection con = null;
		PreparedStatement ps = null;
		int rowsEffected = 0;
		// preparing sql query for inserting a record
		String sql = "insert into AdminDetails(adminId, adminName,password,gender,dateOfBirth,mailId,phoneNumber) values(?,?,?,?,?,?,?)";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			// setting values to the place holders of query
			ps.setInt(1, admin.getAdminId());
			ps.setString(2, admin.getAdminName());
			ps.setString(3, admin.getPassword());
			ps.setString(4, admin.getGender());
			ps.setDate(5, new Date(admin.getDateOfBirth().getTime()));
			ps.setString(6, admin.getMailId());
			ps.setString(7, admin.getPhoneNum());
			// executing sql query
			rowsEffected = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return (rowsEffected > 0) ? true : false;
	} // End of insertAdminRecord

//	/**
//	 * This is the implementation of AdminDAO updateAdminRecord(AdminBean admin)
//	 * method. This method updates a admin record in table.
//	 */
//	@Override
//	public boolean updateAdminRecord(AdminBean admin) {
//		Connection con = null;
//		PreparedStatement ps = null;
//		int rowsEffected = 0;
//		// preparing sql query for updating records
//		String sql = "update AdminDetails set adminName = ?,password = ?,gender = ?, dateOfBirth = ?, mailId = ?, phoneNum = ? where adminId = ?";
//		try {
//			// getCon is a static method which returns Connection Object
//			con = DButil.getcon();
//			ps = con.prepareStatement(sql);
//			// setting values into the place holders
//			ps.setString(1, admin.getAdminName());
//			ps.setString(2, admin.getPassword());
//			ps.setString(3, admin.getGender());
//			ps.setDate(4, (Date) admin.getDateOfBirth());
//			ps.setString(5, admin.getMailId());
//			ps.setString(6, admin.getPhoneNum());
//			ps.setInt(7, admin.getAdminId());
//			// executing sql query
//			rowsEffected = ps.executeUpdate();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				// closing connection
//				con.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
//		return (rowsEffected > 0) ? true : false;
//	} // End of updateAdminRecord

	/**
	 * This method fetches record based on id and sets data to the AdminBean object.
	 */
	public AdminBean fetchRecordById(int adminId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AdminBean admin = new AdminBean();
		// preparing sql query for updating records
		String sql = "select * from AdminDetails where adminId = ?";
		try {
			// getCon is a static method which returns Connection Object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			// setting values into the place holders
			ps.setInt(1, adminId);
			// executing sql query
			rs = ps.executeQuery();
			//preparing AdminBean object
			if(rs.next()) {
				admin.setAdminId(rs.getInt(1));
				admin.setAdminName(rs.getString(2));
				admin.setPassword(rs.getString(3));
				admin.setGender(rs.getString(4));
				admin.setDateOfBirth(rs.getDate(5));
				admin.setMailId(rs.getString(6));
				admin.setPhoneNum(rs.getString(7));
			}
		} catch (Exception e) {

		}

		return admin;
	} // End of getRecordById
	
	public boolean updatePassword(int adminId,String password) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "update admindetails set password = ? where adminId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, password);
			ps.setInt(2, adminId);
			int rs = ps.executeUpdate();
			if(rs>0) {
				result = true;
				}
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return result;
	}


	@Override
	public boolean updateName(int adminId, String adminname) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "update admindetails set adminname = ? where adminId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, adminname);
			ps.setInt(2, adminId);
			int rs = ps.executeUpdate();
			if(rs>0) {
				result = true;
				}
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return result;
	}


	@Override
	public boolean updateDate(int adminId, java.util.Date dateOfBirth) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		AdminBean admin = new AdminBean();
		String sql = "update admindetails set dateOfBirth = ? where adminId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setDate(1, (Date) admin.getDateOfBirth());
			ps.setInt(2, adminId);
			int rs = ps.executeUpdate();
			if(rs>0) {
				result = true;
				}
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return result;
	}


	@Override
	public boolean updateGender(int adminId, String gender) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "update admindetails set gender = ? where adminId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, gender);
			ps.setInt(2, adminId);
			int rs = ps.executeUpdate();
			if(rs>0) {
				result = true;
				}
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return result;
	}

	


	@Override
	public boolean updateMailId(int adminId, String mailId) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "update admindetails set mailId = ? where adminId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, mailId);
			ps.setInt(2, adminId);
			int rs = ps.executeUpdate();
			if(rs>0) {
				result = true;
				}
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return result;
	}



	@Override
	public boolean updatePhoneNumber(int adminId, String phoneNum) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "update admindetails set phoneNum = ? where adminId = ? ";
		try {
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, phoneNum);
			ps.setInt(2, adminId);
			int rs = ps.executeUpdate();
			if(rs>0) {
				result = true;
				}
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return result;
	}


	@Override
	public boolean updateAdminRecord(AdminBean admin) {
		// TODO Auto-generated method stub
		return false;
	}


	


} // End of AdminDAOImpl


