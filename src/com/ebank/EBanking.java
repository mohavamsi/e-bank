package com.ebank;

import java.util.Scanner;

import com.ebank.controller.AdminControllerImpl;
import com.ebank.controller.UserControllerImpl;

/**
 * Main class contains admin and user navigations.
 * 
 * @author BATCH-E
 *
 */
public class EBanking {
	public static void main(String[] args) {
		// calling startApp method
		new EBanking().startApp();

	}// end of main

	/**
	 * From this method execution onwards you can see the Application working. this
	 * method asks the user for user login or administrator login and navigates
	 * respectively.
	 * 
	 * calling this method prints home page menu
	 * 
	 * @throws InvalidInputException
	 */
	public void startApp() {

		// Scanner class is intialiazed
		Scanner sc = new Scanner(System.in);
		System.out.println("********Menu**********\n1.Admin\n2.User");
		System.out.println("enter your choice:");
		String choice = sc.next();
		switch (choice) {
		case "1":
			AdminControllerImpl admin = new AdminControllerImpl();
			admin.adminNavigator();
			break;
		case "2":
			UserControllerImpl user = new UserControllerImpl();
			user.userLogin();
			break;
		default:
			System.out.println("Enter a valid input");
		}

	}
}
