package com.ebank.service;

import java.util.Date;

import com.ebank.beans.AdminBean;
import com.ebank.dao.AdminDAO;
import com.ebank.dao.AdminDAOImpl;

/**
 * This class provides implementation for functions of AdminService interface
 * 
 * @author Team-E
 *
 */
public class AdminServiceImpl implements AdminService {

	/**
	 * This is implementation of AdminService doRegistration(AdminBean) method. This
	 * method perform registration of admin.
	 */
	public boolean doRegistration(AdminBean admin) {
		AdminDAO adminDao = new AdminDAOImpl();
		// calling AdminDAO method
		boolean isRegistered = adminDao.insertAdminRecord(admin);
		return isRegistered;
	} // End of doRegistration

	/**
	 * This is implementation of AdminService updateAdminDetails(AdminBean) method.
	 * This method performs update operation on Admin Details.
	 */

	public boolean updateAdminDetails(AdminBean admin) {
		AdminDAO adminDao = new AdminDAOImpl();
		// calling AdminDAO method
		boolean isUpdated = adminDao.updateAdminRecord(admin);
		return isUpdated;
	} // End of updateAdminDetails

	/**
	 * This is the implementation of AdminService getAdminById(int) method. This
	 * method retrieves AdminBean Object based on adminId.
	 */
	@Override
	public AdminBean getAdminById(int adminId) {

		AdminDAO adminDao = new AdminDAOImpl();
		AdminBean admin = adminDao.fetchRecordById(adminId);
		return admin;
	} // End of getAdminById

	// returns true if record is updated successfully
	public boolean updatePassword(int adminId, String password) {
		AdminDAO adminDao = new AdminDAOImpl();
		boolean result = adminDao.updatePassword(adminId, password);
		return result;
	}// End of updatePassword

	// returns true if record is updated successfully
	public boolean updateName(int adminId, String name) {
		AdminDAO adminDao = new AdminDAOImpl();
		boolean result = adminDao.updateName(adminId, name);
		return result;
	}// End of Updatename

	// returns true if record is updated successfully
	public boolean updateDate(int adminId, Date dateOfBirth) {
		AdminDAO adminDao = new AdminDAOImpl();
		boolean result = adminDao.updateDate(adminId, dateOfBirth);
		return result;
	}// End of updateDate

	// returns true if login is successfull
	public boolean doLogin(int adminId) {
		AdminDAO adminDAO = new AdminDAOImpl();
		boolean result = adminDAO.doLogin(adminId);
		return result;

	}// End of doLogin

	/**
	 * This is the implementation of AdminService doLogin(int, String) method. This
	 * method perform login operation for an admin if authentication is success.
	 */

	public boolean doLogin(int adminId, String password) {
		AdminDAO adminDAO = new AdminDAOImpl();
		boolean result = adminDAO.isRecordExist(adminId, password);
		return result;
	}

	// returns true if record is updated successfully
	public boolean updateGender(int adminId, String gender) {
		AdminDAO adminDao = new AdminDAOImpl();
		boolean result = adminDao.updateGender(adminId, gender);
		return result;
	}

	// returns true if record is updated successfully
	public boolean updateMailId(int adminId, String mailId) {
		AdminDAO adminDao = new AdminDAOImpl();
		boolean result = adminDao.updateMailId(adminId, mailId);
		return result;
	}

	// returns true if record is updated successfully
	public boolean updatePhoneNumber(int adminId, String phoneNum) {
		AdminDAO adminDao = new AdminDAOImpl();
		boolean result = adminDao.updatePhoneNumber(adminId, phoneNum);
		return result;
	}

}
