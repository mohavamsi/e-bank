package com.ebank.service;

import java.util.Date;
import java.util.List;

import com.ebank.beans.AccountBean;
import com.ebank.beans.UserBean;
import com.ebank.dao.UserDAO;
import com.ebank.dao.UserDaoImpl;

public class UserServiceImpl implements UserService {

	// This method is used for user login
	public boolean doLogin(int userId) {
		UserDAO userDAO = new UserDaoImpl();
		boolean result = userDAO.doLogin(userId);
		return result;

	}

	// this method is used for user to login
	public boolean doLogin(int userId, String password) {
		UserDAO userDAO = new UserDaoImpl();
		boolean result = userDAO.isRecordExists(userId, password);
		return result;
	}

	// this method is used to register the new user
	public boolean doRegistration(UserBean user,AccountBean account) {

		UserDAO userDAO = new UserDaoImpl();
		boolean result = userDAO.insertUserRecord(user);
		if(result) {
			
			boolean result1=userDAO.insertAccountRecord(account);
			if(result1) {
				System.out.println("Account added successfully");
			}
		}
		return result;
	}
	
	public boolean doRegistrationUser(UserBean user) {

		UserDAO userDAO = new UserDaoImpl();
		boolean result = userDAO.insertUserRecord(user);
		
		return result;
	}
	
	public boolean doRegistrationAccount(AccountBean account) {

		UserDAO userDAO = new UserDaoImpl();
		boolean result = userDAO.insertAccountRecord(account);
		
		return result;
	}

	// this method is used to display all the users in the table
	public List<UserBean> fetchAllUsers() {
		UserDAO userDAO = new UserDaoImpl();
		List<UserBean> userList = userDAO.getAllUsers();
		return userList;
	}

	/**
	 * This is the implementation of UserService getUserById(int) method. This
	 * method retrieves UserBean Object based on userId.
	 */

	public UserBean getUserById(int userId) {
		UserDAO userDao = new UserDaoImpl();
		UserBean user = userDao.fetchRecordById(userId);
		return user;
	} // End of getUserById

	// This method updates already existing user details
	public boolean updatePassword(int userId, String password) {
		UserDAO userDao = new UserDaoImpl();
		boolean result = userDao.updatePassword(userId, password);
		return result;
	}

	// This method updates already existing user details
	public boolean updateFirstName(int userId, String firstName) {
		UserDAO userDao = new UserDaoImpl();
		boolean result = userDao.updateFirstName(userId, firstName);
		return result;
	}

	// This method updates already existing user details
	public boolean updateLastName(int userId, String lastName) {
		UserDAO userDao = new UserDaoImpl();
		boolean result = userDao.updateLastName(userId, lastName);
		return result;
	}

	// This method updates already existing user details
	public boolean updatePhoneNumber(int userId, String phoneNum) {
		UserDAO userDao = new UserDaoImpl();
		boolean result = userDao.updatePhoneNumber(userId, phoneNum);
		return result;
	}

	// This method updates already existing user details
	public boolean updateMailId(int userId, String mailId) {
		UserDAO userDao = new UserDaoImpl();
		boolean result = userDao.updateMailId(userId, mailId);
		return result;
	}

	// This method updates already existing user details
	public boolean updateDateOfBirth(int userId, Date dateOfBirth) {
		UserDAO userDao = new UserDaoImpl();
		boolean result = userDao.updateDateOfBirth(userId, dateOfBirth);
		return result;
	}

	// This method updates already existing user details
	public boolean updateGender(int userId, String gender) {
		UserDAO userDao = new UserDaoImpl();
		boolean result = userDao.updateGender(userId, gender);
		return result;
	}

	/**
	 * This method contains logic to get account details of specific user based on userId
	 */
	@Override
	public AccountBean getAccountdetails(int userId) {
		
		return new UserDaoImpl().fetchAccountByUserId(userId);
	} // End of getAccountdetails

}
