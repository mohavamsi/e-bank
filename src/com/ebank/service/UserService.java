package com.ebank.service;

import java.util.Date;
import java.util.List;

import com.ebank.beans.AccountBean;
import com.ebank.beans.AdminBean;
import com.ebank.beans.UserBean;
/**
 * This interface contains function declarations which are done by admin
 * 
 * @author Team-E
 *
 */
public interface UserService {
	/**
	 * This method authenticates user, if success let the user login.
	 * @param userId
	 * @param password
	 * @return true if login successful else false
	 */
	boolean doLogin(int userId);
	
	/**
	 * This method authenticates user, if success let the user login.
	 * @param userId
	 * @param password
	 * @return true if login successful else false
	 */
	boolean doLogin(int userId, String password);
	
	/**
	 * This method performs registration of user
	 * @param user
	 * @return true if registration success else false
	 */
	boolean doRegistration(UserBean user,AccountBean account);
	/**
	 * It is used to return all the records in tableSS
	 * @return list of users present in the given table
	 */
	
	
	public List<UserBean> fetchAllUsers();

	/**
	 * This method retrieves particular user Object from table
	 * 
	 * @param userId
	 * @return UserBean
	 */
	UserBean getUserById(int userId);
	
	/**
	 * This method updates already existing user details
	 * @param userId
	 * @param password
	 * @return true if updation is success else false
	 */
	boolean updatePassword(int userId, String password);
	
	/**
	 * This method updates already existing user details
	 * @param userId
	 * @param firstName
	 * @return true if updation is success else false
	 */
	 boolean updateFirstName(int userId, String firstName) ;
	 
	 /**
	  * This method updates already existing user details
	  * @param userId
	  * @param lastName
	  * @return true if updation is success else false
	  */
	boolean updateLastName(int userId, String lastName) ;
	
	/**
	 * This method updates already existing user details
	 * @param userId
	 * @param phoneNum
	 * @return true if updation is success else false
	 */
	 boolean updatePhoneNumber(int userId, String phoneNum);
	 
	 /**
	  * This method updates already existing user details
	  * @param userId
	  * @param mailId
	  * @return true if updation is success else false
	  */
	 boolean updateMailId(int userId, String mailId);
	 
	 /**
	  * This method updates already existing user details
	  * @param userId
	  * @param dateOfBirth
	  * @return true if updation is success else false
	  */
	 boolean updateDateOfBirth(int userId, Date dateOfBirth) ;
	 
	 /**
	  * This method updates already existing user details
	  * @param userId
	  * @param gender
	  * @return true if updation is success else false
	  */
	 boolean updateGender(int userId, String gender);
	 
	 /**
	  * This method retrieves account details based on userId
	  * @param userId
	  * @return
	  */
	 AccountBean getAccountdetails(int userId);

	boolean doRegistrationUser(UserBean user);

	boolean doRegistrationAccount(AccountBean account);
	 
}
