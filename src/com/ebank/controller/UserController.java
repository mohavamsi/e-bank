package com.ebank.controller;

import java.util.List;

import com.ebank.beans.UserBean;
import com.ebank.beans.UserTransactionBean;

/**
 * This interface contains function declarations which are done by user
 * 
 * @author Team-E
 *
 */
public interface UserController {

	/**
	 * This method is used for registering new User into User Table
	 */
	public void userRegistration();

	/**
	 * This method is used for Logging in User by using userId and password
	 */
	public void userLogin();

	/**
	 * This method retrieves balance details from account
	 * 
	 * @param accNum
	 */
	void doBalanceEnquiry(int accNum);

	/**
	 * This method performs deposit operation
	 * 
	 * @param accNum
	 */
	void doDeposit(int accNum);

	/**
	 * This method performs withdraw operation
	 * 
	 * @param accNum
	 */
	void doWithdraw(int accNum);

	/**
	 * This method performs transferring amount between two accounts
	 * 
	 * @param accNum
	 */
	void doTransfer(int accNum);
	
	/**
	 * This method performs operations done by user like deposit,withdraw etc..
	 * @param userId
	 */
	void bankOperations(int userId);
	
	/**
	 * This method is used for paying  the bills from the user account
	 * @param accNum
	 */
	void billPayment(int accNum);
	
	/**
	 * 
	 * @param userId
	 * @return user object to update a particular field in the user table.
	 */
	public UserBean updateUserDetails(int userId);
	
	/**
	 * This method retrieves transaction History of user for a particular account
	 * number
	 * 
	 * @param accNum
	 */
	void getTransactionHistory(int accNum);

	/**
	 * This method prints transaction history on the console
	 * 
	 * @param txList
	 */
	void printTransaction(List<UserTransactionBean> txList);

	/**
	 * This method used to get last five transactions of account as mini statement
	 * 
	 * @param accNum
	 */
	void getMiniStatement(int accNum);

}
