package com.ebank.beans;

import java.util.Date;

/**
 * @author Team-E This class is an utility class representing Account Objects
 *         which implements Serialazation
 *
 */
public class AccountBean {

	private int accountNumber;
	private String accountHolderName;
	private double balance;
	private String nomineeName;
	private String typeOfAccount;
	private Date openingDate;
	private boolean status;
    private int userId;
	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId; 
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountHolderNmae() {
		return accountHolderName;
	}

	public void setAccountHolderNmae(String accountHolderNmae) {
		this.accountHolderName = accountHolderNmae;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getTypeOfAccount() {
		return typeOfAccount;
	}

	public void setTypeOfAccount(String typeOfAccount) {
		this.typeOfAccount = typeOfAccount;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String toString() {
		return "Account Number=" + accountNumber + "\nAccount Holder Name=" + accountHolderName + "\nbalance=" + balance
				+ "\nNominee Name=" + nomineeName + "\nType Of Account=" + typeOfAccount + "\nOpening Date="
				+ openingDate + "\nStatus=" + status + "\n\n";
	}
}
