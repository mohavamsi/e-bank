package com.ebank.beans;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Team-E This class is an utility class representing User Complaints
 *         Objects which implements Serialazation
 *
 */
public class ComplaintsBean {

	private int userId;
	private int complaintId;
	private String description;
	private Timestamp dateOfComplaint;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getComplaintId() {
		return complaintId;
	}

	public void setComplaintId(int complaintId) {
		this.complaintId = complaintId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getDateOfComplaint() {
		return dateOfComplaint;
	}

	public void setDateOfComplaint(Timestamp dateOfComplaint) {
		this.dateOfComplaint = dateOfComplaint;
	}

	@Override
	public String toString() {
		return "ComplaintsBean userId=" + userId + ", complaintId=" + complaintId + ", description=" + description
				+ ", dateOfComplaint=" + dateOfComplaint + "\n\n";
	}

}
